
# **Aclaraciones**

* La duración de este módulo es de 1 mes y los plazos comienzan a correr desde la asignación del mismo.
* Durante el tiempo que lleve la cursada, se llevarán adelante clases sincrónicas a las cuales la persona cursante deberá unirse y se dará aviso con antelación sobre las mismas.
