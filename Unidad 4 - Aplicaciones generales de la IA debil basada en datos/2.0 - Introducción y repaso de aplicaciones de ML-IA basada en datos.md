
# **Introducción y repaso de aplicaciones de ML / IA basada en datos**

## **Algunas aplicaciones**

![AA](img/Algunas_aplicaciones.png "AA")

![ML](img/Machine_learning.png "ML")

## **Cuestiones generales del aprendizaje supervisado**

[Ver video](https://youtu.be/bQI5uDxrFfA "CGAS")

## **Proyectos disponibles de TensorFlow**

![PD](img/Proyectos_disponibles.png "PD")

## **Repaso sobre proyectos de Software y su documentación**

Un proyecto es una **secuencia de actividades unicas, complejas e interconectados** que tienen una meta o proposito y que deben completarse en un **tiempo y presupuesto** estipulado, y de acuerdo a una **especificacion**.

* Secuencia: tareas a realizarse en algun orden.
* Unicas: "Un proyecto es una experiencia unica que nunca ha sucedido antes y jamas volvera a suceder."
* Complejas: En general son conjuntos de actos no triviales.
* Interconectadas: Dependencias entre actividades.
* Tiempo y presupuesto: Segun estimaciones.
* Especificacion: Requerimientos (funcionales y no funcionales) acordados con el cliente.

### ¿Por que documentar?

* La documentacion es ampliamente reconocida como una componente fundamental de **procesos maduros** de desarrollo y mantenimiento de todo tipo de **software**.
* Los proyectos de IA/aprendizaje automatizado/ciencia de datos son **casos particulares de proyectos de software**.
* Comencemos entonces repasando muy brevemente el modelo de calidad de productos de software incluido en la norma ISO/IEC:
    - Norma de calidad internacional para sentar las bases de una evaluacion integral de los diversos aspectos que hacen a la calidad de productos.
    - Esta informada por modelos desarrollados en los años 1970s.
    - Fue aprobada en 1991 y, luego de 20 años, reemplazada por la ISO/IEC 25010.
    - Aqui vemos la "clasica" 9126 ya que su evolucion no cambia en los aspectos resaltados.

## **Modelo de Calidad de Software ISO 9126**

![MCS](img/Modelo_de_calidad_de_software.png "MCS")

![M1](img/Mantenibilidad_1.png "M1")
![M2](img/Mantenibilidad_2.png "M2")
![M3](img/Mantenibilidad_3.png "M3")

## **Elementos esenciales de una buena documentación**

¿Cuales son? ¿Cuales son los diferentes actores que la consumen? ¿Son todas siempre necesarias?

![CDP](img/Componentes_de_la_documentacion_de_un_proyecto.png "CDP")

### **Descripcion breve**

* Muy breve descripcion, un par de frases acerca de lo que hace **y nada mas**.
    - Es lo primero que ve el usuario, y le da una idea acerca de que espera.
    - Sera lo que usen los buscadores para indexar, por lo que ser conciso y preciso es importante.
* Tipicamente aparecen al principio en las paginas de repositorios, o en las primeras lineas de un archivo "readme".
* Ejemplos:

![DB](img/Descripcion_breve.png "DB")

### **"Getting started" (Guia para comenzar)**

* Aqui se incluye:
    - Un ejemplo pequeño, generalmente lo **mas basico posible** (en la jerga informatica, se lo suele llamar "Hello World").
    - Si es necesario, una descripcion de pasos a seguir para instalar las componentes.
* La descripcion breve (Paso 1) es el que, mientras que esta guia es el "como"
* Puede considerarse una de las partes mas importantes, ya que le da a los usuarios un pantallazo de como es el software, y como se ajusta a las necesidades del usuario que lo esta investigando.
* Ejemplo en React:

![HW](img/Hello_world.png "HW")

### **Tutoriales**

* En esta seccion se incluyen instrucciones para usar el software para **casos de uso tipicos**.
* El formato que se suele seguir es una porcion breve de instrucciones en lenguaje natural, seguidas de porciones de codigo (que reciben el nombre de "snippets").
* Tambien es comun encontrar links, screenshots y videos embebidos para ilustrar los diferentes conceptos que se quieren comunicar.
* Ejemplo 

![Tutoriales](img/Tutoriales.png "Tutoriales")

### **Proyectos de ejemplo**

![PE](img/Proyectos_de_ejemplo.png "PE")

### **API: Interfaces expuestas**

![API1](img/API_1.png "API1")

![API2](img/API_2.png "API2")

### **Descripcion arquitectonica**

![DA](img/Descripcion_arquitectonica.png "DA")

## **Documentación "buena"**

El camino hacia la mejora:

* Es un proceso continuo; no se puede esperar hacerlo perfecto en el primer intento.
* Se basa en metricas, como vimos anteriormente:
    - Metricas objetivas: basadas en tamaño, densidades, cobertura, etc.
    - Metricas subjetivas: basadas en feedback de sus consumidores ("¿Le resulto util esta?")

Hay una confusion (grave) que se usa el **Manifiesto Agil** para justificar la falta de documentacion:

* "Hemos aprendido a valorar [...] software funcionando sobre documentacion extensiva [...]"
* Esto significa que la documentacion no garantiza el exito de un proyecto y que se debe minimizar el desperdicio en documentar de mas, no que **no haya que documentar**.

## **Organización y documentación de proyectos de machine learning**

¿ Como es el ciclo de vida de dasarrollo basado en ML ? ¿ Que aspectos particulares surgen para documentar ?

### **Ciclo de vida de proyectos basados en aprendizaje automatizado**

![CVML](img/Ciclo_de_vida_ML.png "CVML")

### **(1) Planificacion y armado del proyecto**

![PAP](img/Planificacion_y_armado_del_proyecto.png "PAP")

#### **Proyectos de ML como "Software 2.0"**

![S2.0](img/Soft_2.0.png "S2.0")

### **(2) Coleccion de datos y etiquetado**

![CDE](img/Coleccion_de_datos_y_etiquetado.png "CDE")

#### **Apartado: Separaciones tipicas de los datos**

![STD1](img/Separacion_tipica_de_los_datos_1.png "STD1")

![STD2](img/Separacion_tipica_de_los_datos_2.png "STD2")

![STD3](img/Separacion_tipica_de_los_datos_3.png "STD3")

![STD4](img/Separacion_tipica_de_los_datos_4.png "STD4")

### **(3) Exploracion de modelos**

![EM1](img/Exploracion_de_modelos_1.png "EM1")

![EM2](img/Exploracion_de_modelos_2.png "EM2")

### **(4) Refinamiento de modelos**

![RM](img/Refinamiento_de_modelos.png "RM")

### **(5) Testeo y evaluacion**

![TE1](img/Testeo_y_evaluacion_1.png "TE1")

![TE2](img/Testeo_y_evaluacion_2.png "TE2")

### **(6) Deployment de modelos**

![DM](img/Deployment_de_modelos.png "DM")

### **(7) Mantenimiento continuo de modelos**

![MCM1](img/Mantenimiento_continuo_de_modelos_1.png "MCM1")

![MCM2](img/Mantenimiento_continuo_de_modelos_2.png "MCM2")

---

## **Roles en los equipos**

![RE](img/Roles_de_los_equipos.png "RE")

## **¿Ingenieros/as de software? ¿Para que?**

![IS](img/Ingenieros_de_soft_para_que.png "IS")