
# **Curso IALAB - Gobernanza de Algoritmos**

Curso de Inteligencia Artificial - Gobernanza de Algoritmos.

## **Unidad 1: Ecosistema de la IA. Conceptualización, regulación, desafíos y problemáticas del Tratamiento automatizado de datos personales**

* Contenido de la Unidad 1 

1. Datos, datos personales y macrodatos
2. Datos personales
3. Datos sensibles
4. RGPD- Tratamiento automatizado y elaboración de perfiles
5. Fases del tratamiento automatizado de datos con IA
6. El impacto del Reglamento General de Protección de Datos (GDPR) en la inteligencia artificial
7. Actualización constante, comparación de perfiles
8. Transmisión y subasta de datos
9. Consentimiento + Muestra de datos personales que son públicos
10. Anonimización de datos personales
11. Orientaciones y garantías en los procedimientos de anonimización de datos personales

## **Unidad 2: Profundizando conceptos y técnicas de la IA. Vinculación con los datos**

* Contenido de la Unidad 2

1. ¿Qué es un algoritmo?
2. Ejemplos de algoritmos cotidianos
3. Ecosistema de la IA (Introducción I)
4. El ecosistema de la IA (Introducción II y marco histórico)
5. Técnicas actuales de IA
6. Consideraciones para la gobernanza según técnica de IA
7. Relación entre personas humanas y evolución tecnológica
8. ¿Qué aprenden los sistemas inteligentes? Clasificación de la IA según sus objetivos (IA débil o acotada; IA general o fuerte; "super inteligencia")
9. Ámbitos de aplicación e impacto que ha tenido la IA en la sociedad global
10. Respuestas inteligentes a problemas emergentes
11. La visión conceptual de alto nivel de la IA
12. ¿Cómo lo hace el aprendizaje automático (machine learning)?
13. Sistemas conexionistas - Representaciones subsimbólicas
14. Algoritmos inteligentes - The Machine Learning Process
15. Relación entre data science y machine learning
16. ¿Qué es un patrón de conocimiento?
17. La ética en el centro de una nueva generación de estándares

* Test de Lectura - Unidades 1 y 2

## **Unidad 3: Ciclo de vida de la Inteligencia Artificial**

* Contenido de la Unidad 3

1. Conceptos básicos de la IA: Ciclo de vida de sistemas de IA
2. IA para mejorar vidas. IA ética
3. Iniciativas para adopción responsable
4. Principios de la OCDE
5. Conceptos básicos de la IA: Auditabilidad
6. Concepto de auditabilidad de los sistemas
7. Los centros de ensayo deben facilitar la auditoría y evaluación independientes de los sistemas de IA
8. Conceptos básicos de la IA: Sesgo
9. Conceptos básicos de la IA: Ética
10. Conceptos básicos de la IA: IA ética e IA centrada en la persona
11. Directrices éticas sobre una Inteligencia Artificial fiable
12. Conceptos básicos de la IA: Reproducibilidad e IA robusta
13. Conceptos básicos de la IA: Trazabilidad
14. Ejemplo de trazabilidad aplicado a sistemas de caja blanca (no basado en redes neuronales) - Parte 1
15. Ejemplo de trazabilidad aplicado a sistemas de caja blanca (no basado en redes neuronales) - Parte 2
16. Ejemplo de trazabilidad aplicado a sistemas de caja blanca (no basado en redes neuronales) - Parte 3
17. Conceptos básicos de la IA: Confianza
18. IA fiable
19. Sesgos en IA fiable
20. Evaluación de IA fiable
21. Principios para IA fiable
22. Cajas blancas vs. Cajas negras
23. Clasificador "caja negra" (ejercicio)
24. ¿Cómo se traduce todo esto en aplicaciones concretas? Caso real con perros y lobos?
25. Cajas negras vs. Explicabilidad: ¿Es necesariamente una dicotomía?
26. Predicciones de caja blanca
27. Peligros: Manipulación vía agregado de ruido
28. Conclusiones
29. Caso ejemplo. Detección inteligente: PretorI

* Test de Lectura

## **Unidad 4: Aplicaciones generales de la Inteligencia Artificial débil basada en datos**

* Contenido de la Unidad 4

1. Introducción y repaso de aplicaciones de ML / IA basada en datos
2. Cuestiones generales del aprendizaje supervisado
3. Ejemplos de machine learning
4. Proyectos disponibles de TensorFlow
5. Repaso sobre proyectos de Software y su documentación
6. Modelo de Calidad de Software ISO 9126
7. Elementos esenciales de una buena documentación
8. Documentación "buena"
9. Organización y documentación de proyectos de machine learning
10. Usos y ejemplos de API (Interfaz de Programación de Aplicaciones)
11. Ejemplos de hiperparámetros

* Test de Lectura

## **Unidad 5: Árboles y bosques aleatorios de decisión**

* Contenido de la Unidad 5

1. Introducción y repaso sobre los distintos tipos de aprendizaje automatizado - Supervisado y No Supervisado
2. Aprendizaje automatizado - Por Refuerzo
3. Aprendizaje representacional ("feature learning")
4. Aprendizaje evolutivo
5. Árboles de decisión (parte 1)
6. Árboles de decisión (parte 2)
7. Árboles de decisión (parte 3)
8. Árboles de decisión en ejemplos
9. Poder de discernimiento de árboles de decisión como función de su altura/profundidad
10. ¿Qué sigue después de la construcción del árbol?
11. Métodos de ensamblado
12. Combinaciones homogéneas vs. heterogéneas
13. Bosques aleatorios de decisión

* Test de Lectura

## **Unidad 6: Gradient descent y regresión logística**

* Contenido de la Unidad 6

1. Introducción al aprendizaje supervisado - R.N.A
2. Regresión, representación de la información y clasificación
3. Regresión lineal con una variable
4. De regresión lineal a "descenso por el gradiente" en IA
5. El descenso por el gradiente
6. El algoritmo del descenso por el gradiente
7. Parámetros o pesos sinápticos en redes neuronales
8. El descenso por el gradiente en Python
9. Regresión lineal múltiple
10. Regresión lineal con múltiples variables
11. Regresión logística
12. Regresión logística o función sigmoide
13. Regresión logística binaria
14. Regresión logística: Performance

## **Unidad 7: Redes neuronales simples**

* Contenido de la Unidad 7

1. Redes neuronales artificiales (parte 1)
2. Redes neuronales artificiales (parte 2)
3. Componentes de un sistema neuronal o conexionista
4. La neurona artificial
5. Arquitectura de las redes neuronales artificiales (1)
6. Aspectos de las redes neuronales artificiales
7. Arquitectura de las redes neuronales artificiales (2)
8. Redes neuronales (Marvin Lee Minsky, 1969)
9. Perceptron simple

* Test de Lectura Unidades 6 y 7

## **Unidad 8: Redes neuronales complejas**

* Contenido de la Unidad 8

1. Seguimos con redes neuronales artificiales (Introducción)
2. Repaso de componentes de un sistema neuronal o conexionista
3. Repaso de tipos de aprendizajes automatizados
4. Repaso y continuación de la neurona artificial
5. Repaso y continuación de redes neuronales (Marvin Lee Minsky, 1969)
6. Perceptron MLP - Backpropagation
7. Definiendo la RNA MLP Backpropagation
8. MLP - Backpropagation: Diseño de modelo
9. La red Backpropagation
10. MLP - Backpropagation: Algoritmo de aprendizaje
11. MLP - Backpropagation: Supresión del gradiente
12. La epistemología de Deep Learning
13. Cierre de MLP - Backpropagation
14. Redes recurrentes: Redes de Hopfield
15. Problema de memoria asociativa: Redes de Hopfield
16. De Redes de Hopfield a Máquinas de Boltzmann
17. Redes recurrentes: Redes de Boltzmann
18. Recurrent Neural Networks
19. La máquina de Boltzmann restringida (1)
20. Red de Boltzmann restringida (RBM)
21. La máquina de Boltzmann restringida (2
22. Restricted Boltzmann Machines
23. Cierre del paradigma estándar de ML "tradicional"

* Test de Lectura

## **Unidad 9: Topic Modeling - Modelado de temas**

* Contenido de la Unidad 9

1. Análisis de textos: Conceptos básicos
2. Tópicos
3. Modelos generativos
4. Modelos discriminatorios vs. generativos
5. Topic Modeling vs. Topic Classification
6. Topic Modeling
7. Topic Frequency-Inverse Document Frequency (TF-IDF)
8. Análisis Latente Semántico (LSA)
9. Singular Value Decomposition truncada
10. Asignación Latente de Dirichlet (LDA)
11. Resumen de técnicas vistas
12. Ejemplos prácticos de Topic Modeling: Proyecto Colombia (parte 1)
13. Ejemplos prácticos de Topic Modeling: Proyecto Colombia (parte 2)
14. Ejemplos prácticos de Topic Modeling: Proyecto Colombia (parte 3)
15. Latent Dirichlet Allocation: Module reference

* Test de Lectura

## **Unidad 10: SVM - Overfitting; Generalización **

* Contenido de la Unidad 10

1. Support Vector Machines (1)
2. Support Vector Machines (2)
3. Intuición del caso simple
4. Intuición para el caso más complejo
5. Funciones Kernel
6. La Navaja de Occam
7. Sesgo/Varianza y Under/Overfitting
8. Recordatorio: Validación cruzada
9. Recordatorio: Boosting y Bagging
10. Ejemplos prácticos de SVM (parte 1)
11. Ejemplos prácticos de funciones Kernel
12. Conclusiones sobre casos prácticos

* Test de Lectura

## **Unidad 11: Aprendizaje no supervisado: K-means; PCA; Redes neuronales: SOM, RBM **

* Contenido de la Unidad 11

1. Técnicas de regularización: Lasso y Ridge en Machine Learning
2. Lasso y Ridge para la regresión lineal
3. Regularización en los módulos de ML que usan el descenso por el gradiente
4. Aprendizaje no supervisado
5. La clave para la próxima revolución de la IA
6. RNA (modelo de Hopfield) recurrentes
7. RNA: RBM (descubrir características importantes)
8. Máquina de Boltzmann restringida - ¿Por qué es un modelo generativo y no discriminativo?
9. Clustering
10. Clustering sin redes neuronales (Data Mining)
11. Clustering con K-Means
12. Algoritmo K-Means
13. Modelo de mapas autoorganizados (SOM) Kohonen - Clustering
14. Caracterización y algoritmo de aprendizaje autoorganizado

## **Unidad 12: Sistemas de recomendación y cuestiones claves acerca del modo de tomar decisiones**

* Contenido de la Unidad 12

1. Introducción a los sistemas de recomendación
2. Sistemas de recomendación
3. Clasificación de sistemas de recomendación
4. Recomendaciones no personalizadas
5. SR personalizados en más detalle
6. Filtrado basado en contenido vs. colaborativo
7. Uno de los desafíos principales: "La cola larga"
8. Desafíos más comunes
9. Filtrado basado en contenido
10. Content Based Recommendations
11. Filtrado colaborativo
12. Collaborative Filtering
13. Basado en usuarios
14. Basado en ítems (IB-CF)
15. Otra opción: CF basado en factorización de matrices
16. CF basado en modelos: Resumen
17. Cierre de filtrado colaborativo y filtrado híbrido
18. Métricas de desempeño
19. Precisión y exhaustividad (recall)
20. Matrices de confusión y métricas derivadas
21. Curvas ROC: Desempeño de la habilidad de diagnóstico
22. Métricas específicas a sistemas de recomendación
23. Relación entre métricas y resumen

* Test de Lectura - Unidades 11 y 12

## **Unidad 13: Recurrent Neural Networks

* Contenido de la Unidad 13

1. Introducción a Deep Learning
2. Deep Learning
3. El ser humano y su ecosistema
4. Animamos objetos inertes, máquinas
5. El Perceptron generalizado
6. Nuevas entidades
7. El Perceptron MLP: Feed forward + Backpropagation
8. Pizarra de MLP
9. ¿Cómo trabaja Deep Learning?
10. Convolutional Neural Network (parte 1)
11. Convolutional Neural Network (parte 2)
12. Convolutional Neural Networks (parte 3)
13. Convolutional Neural Networks (parte 4)
14. Convolutional Neural Networks (parte 5)
15. Convolutional Neural Networks (parte 6)
16. Generative Adversarial Networks (parte 1)
17. Generative Adversarial Networks (parte 2)
18. Técnicas de reconocimiento facial
19. Neural Networks. Reinforcement Learning
20. ¡Amenazantes!

* Test de Lectura