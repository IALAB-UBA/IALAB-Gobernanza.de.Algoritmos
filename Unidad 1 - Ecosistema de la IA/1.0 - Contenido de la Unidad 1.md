
## **Contenido de la Unidad 1**

En esta Unidad encontrarás:

* 3 videos
* 8 lecturas

Aclaraciones e información importante:

```sh
Profesores/as de la Unidad: Pablo Mlynkiewicz y Laura Díaz Dávila.
Deberás completar todos los videos y/o lecturas correspondientes para pasar a la siguiente Unidad.
Recuerda que puedes realizar tus consultas, sin excepción, en las clases sincrónicas.
```
