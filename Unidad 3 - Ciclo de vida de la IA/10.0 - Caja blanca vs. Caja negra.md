
#  **Cajas blancas vs. Cajas negras**

## **Cajas negras**

* En el lenguaje ingenieril, se llama **caja negra** a una forma de ver a un sistema unicamente en funcion de usus entradas y salidas.

![Cajas negras](img/Cajas_negras.png "Cajas negras")

* Ejemplos:
    - Algoritmos, Modelos, Sistemas completos.
    - Tambien cosas mas mundanas: Oficinas, negocios, etc.
* **Importante**: "Caja negra" es una vista y utilizar ese concepto no necesariamente implica que los contenidos del sistema no son accesibles. Ejemplo:
    - Un auto es generalmente visto por sus usuarios como una caja negra.
    - Entradas: Pedales, volante, palanca de cambio, freno de mano, parrillas de luces, guiño, aire acondicionado, etc.ç
    - Salidas: Velocidad de ruedas, andgulo del tren delantero, estado de luces, limpiaparabrisas, etc.
    - Claramente para los mecanicos no se trata de una caja negra.

## **Cajas blancas**

* En contraparte, se llama **caja blanca** a la vista de un sistema que nos da **acceso total** a su funcionmiento.

![Cajas blancas](img/Cajas_blancas.png "Cajas blancas")

* ¡Ojo! Tener acceso al funcionamiento de la "caja" no significa necesariamente que **cualquier persona** podra comprenderlo...
* Siguiendo el ejemplo anterior del auto, para una mecanica este es una caja blanca, ya que puede:
    - diagnosticar problemas,
    - arreglar desperfectos,
    - hacerle mantenimiento, etc.
* En particular, para un usuario comun, un auto es mas bien algo intermedio, ya que tiene un conocimiento basico de como funciona y como arreglar desperfectos menores.
* Prodriamos hablar entonces de cajas grises.

## **Cajas grises**

* Un concepto usado en menor medida es el de **caja gris**, el cual se define como un punto intermedio entre la opacidad y el acceso total.

![Cajas grises 1](img/Cajas_grises_1.png "Cajas grises 1")

* Una forma de concebir a una caja gris es siguiendo el ejemplo del auto, en el que los usuarios comunes tenemos un acceso/compresion solo hasta cierto punto.
* Otra es mediante una **combinacion** de cajas negras y cajas blancas.
* Actualmente, este metodo se esta usando en I+D de sistemas inteligentes para aprovechar las ventajas del **aprendizaje automatizado** y los abordajes basados en **logica computacional**.

![Cajas grises 2](img/Cajas_grises_2.png "Cajas grises 2")

* Veamos un ejemplo...

![Ejemplo de caja gris](img/Ejemplo_de_caja_gris.png "Ejemplo de caja gris")

## **Clasificador "caja negra" (ejercicio)**

¿Como se ve ene realidad un clasificador "caja negra"? Veamos un ejemplo/juego para ilustrarlo...

Vamos a ver una serie de imagenes abstractas que correspondena categorias "A", "B" o "ninguna de las dos":

![Juego clasificador](img/Juego_clasificador.png "Juego clasificador")


**Respuesta** : Podemos decidir que la ultima iamgen corresponde a la categoria tipo "A", porque vemos que el patron 'circulo rojo' se repite en las categorias "A", el patron 'circulo amarillo' se retipe en las de categoria "B" y en la categoria "ninguna de las dos" estos patrones de "circulo rojo o amarillo" no se cumplen.

Parte de la dificultad en darse cuenta de estas definiciones es que **solo importan los circulos**, y nada mas del **contexto**. Quizas si las imagenes fueran menos abstractas, no seria tan dificil.

---

![Clasificador](img/Clasificador.png "Clasificador")

* La **experiencia humana** hace que esto sea trivial - es claro que la mano, posicion, mesa, etc. **son irrelevantes**.
* LOs algoritmos de aprendizaje (¿aun?) no tienen esta facilidad.

## **¿Cómo se traduce todo esto en aplicaciones concretas? Caso real con perros y lobos**

**Caso real**: Clasificador entrenado para disernir entre **lobos** y **perros**. En particular, la dificultad esta en los perros de raza Husky...

Se desarrollo un clasificador con 90% de precision (pero, ¿Fue realmente asi?)

![CPL_1](img/Clasificador_perros_lobos_1.png "CPL_1")

![CPL_2](img/Clasificador_perros_lobos_2.png "CPL_2")

## **Cajas negras vs. Explicabilidad: ¿Es necesariamente una dicotomía?**

Veamos alguna ideas para mitigar los problemas de las cajas negras...

![Idea1](img/Idea1.png "Idea1")

![Idea2](img/Idea2.png "Idea2")

![Idea3](img/Idea3.png "Idea3")

![Idea4](img/Idea4.png "Idea4")

Si bien no es particular a las cajas negras, estas son particularmente vulnerables a los llamados **ataques adversariales**...
